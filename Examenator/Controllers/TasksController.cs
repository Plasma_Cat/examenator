﻿using Examenator.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Examenator.Controllers
{
    public class TasksController : Controller
    {
        TaskRepository taskRepository;

        public TasksController(TaskRepository taskRepository)
        {
            this.taskRepository = taskRepository;
        }

        //task list:    GET     /tasks
        //Calc:         POST    /tasks/4 

        [HttpGet]
        [Authorize]
        public IActionResult GetList()
        {
            return View(taskRepository.Tasks);
        }

        [HttpPost]
        public IActionResult Calc(int taskNum, string input)
        {
            var task = taskRepository.Tasks.FirstOrDefault(x => x.Number == taskNum);
            if (task != null)
            {
                try
                {
                    var result = task.Calculate(input);
                    return Content(result);
                }
                catch (Exception ex)
                {
                    Response.StatusCode = 400;
                    return Content(ex.Message);
                }
            }

            Response.StatusCode = 400;
            return Content($"Task {taskNum} not found.");
        }
    }
}
