﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examenator.Tasks
{
    public interface ITask
    {
        int Number { get; }
        string InputText { get; }
        string AlgorithmText { get; }
        string OutputText { get; }

        string Calculate(string input);      
    }
}
