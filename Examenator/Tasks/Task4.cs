﻿using System;
using System.Collections.Generic;

namespace Examenator.Tasks
{
    public class Task4 : ITask
    {
        public int Number { get; } = 4;
        public string InputText { get; } = "Целое число от 0 до 2048.";
        public string AlgorithmText { get; } = "перевод числа в двоичную систему.";
        public string OutputText { get; } = "результат алгоритма.";

        public string Calculate(string input)
        {
            var error = Validate(input);
            if (!string.IsNullOrEmpty(error))
                throw new ArgumentException(error, nameof(input));

            var value = int.Parse(input);
            return Convert.ToString(value, 2);
        }

        private string Validate(string input)
        {
            if (int.TryParse(input, out var temp))
            {
                if (2048 >= temp && temp >= 0)
                    return null;

                return "Input out of [0;2048] range.";
            }

            return "Input is not a number.";
        }
    }
}
