﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examenator.Tasks
{
    public class Task5 : ITask
    {
        public int Number { get; } = 5;
        public string InputText { get; } = "Строка, разделенная символом \"/\" (например, логин/пароль)";
        public string AlgorithmText { get; } = "Сохранение пользователя в системе.";
        public string OutputText { get; } = " Число символов в пароле.";
               
        public string Calculate(string input)
        {
            var error = Validate(input);
            if (!string.IsNullOrEmpty(error))
                throw new ArgumentException(error, nameof(input));

            var slashPosition = input.IndexOf('/');
            var password = input.Substring(slashPosition + 1);

            return password.Length.ToString();
        }

        private string Validate(string input)
        {
            if (!input.Contains('/'))
            {
                return "Строка не содержит символ /";
            }

            return null;
        }
    }
}
