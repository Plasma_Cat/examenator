﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Examenator.Tasks
{
    public class TaskRepository
    {
        public List<ITask> Tasks { get; }

        public TaskRepository()
        {
            var allTaskTypes = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(x => typeof(ITask).IsAssignableFrom(x) && !x.IsInterface);

            var allTasks = allTaskTypes.Select(x => Activator.CreateInstance(x));
            var getTaskNumber = new Func<object, int>((x) => (int)x.GetType().GetProperty(nameof(ITask.Number)).GetValue(x));
            var orderedTasks = allTasks.OrderBy(getTaskNumber);

            Tasks = orderedTasks.Cast<ITask>().ToList();
        }
    }
}
